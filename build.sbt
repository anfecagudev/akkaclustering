name := "udemy-akka-remoting-clustering"

version := "0.1"

scalaVersion := "2.13.7"

lazy val akkaVersion = "2.6.17"
lazy val protobufVersion = "3.6.1"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-remote" % akkaVersion,
  "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
  "com.typesafe.akka" %% "akka-cluster-sharding" % akkaVersion,
  "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion,
  "io.aeron" % "aeron-driver" % "1.37.0",
  "io.aeron" % "aeron-client" % "1.37.0",
  "com.typesafe.akka" %% "akka-serialization-jackson" % akkaVersion
)