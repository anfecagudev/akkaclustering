package clustering

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSelection, ActorSystem, Address, Props}
import akka.cluster.Cluster
import akka.cluster.ClusterEvent.{InitialStateAsEvents, MemberEvent, MemberRemoved, MemberUp, UnreachableMember}
import akka.util.Timeout
import clustering.ChatDomain.UserMessage
import com.typesafe.config.ConfigFactory
import playground.remoting.MySerializer

import scala.util.{Failure, Success}

object ChatDomain {
  case class ChatMessage(nickname: String, contents: String) extends MySerializer

  case class UserMessage(contents: String, nickname: String) extends MySerializer

  case class EnterRoom(fullAddress: String, nickname: String) extends MySerializer
}


object ChatActor {
  def props(nickname:String, port: Int) = Props(new ChatActor(nickname, port))
}
class ChatActor(nickname: String, port: Int) extends Actor with ActorLogging {

  import ChatDomain._
  import scala.concurrent.duration._
  import scala.language.postfixOps
  import context.dispatcher

  implicit val timeout = Timeout(3 seconds)
  implicit var chatters: Map[Address, ActorRef] = Map()
  implicit var pendingRemovals: Map[Address, ActorRef] = Map()


  val cluster = Cluster(context.system)

  override def preStart(): Unit =
    cluster.subscribe(
      self,
      initialStateMode = InitialStateAsEvents,
      classOf[MemberEvent],
      classOf[UnreachableMember]
    )

  override def postStop(): Unit = cluster.unsubscribe(self)

  override def receive: Receive = {
    case MemberUp(member) =>
      log.info(s"Member ${member.address} has joined the party")
      val fullAddress = s"${member.address}/user/chatActor"
      val actorSel = context.actorSelection(fullAddress)
      actorSel.resolveOne().onComplete {
        case Success(ref) =>
          chatters = chatters + (member.address -> ref)
          ref ! EnterRoom(fullAddress, nickname)
        case Failure(exception) => s"Couldn't get the actorRef because of : $exception"
      }
    case MemberRemoved(member, _) =>
      log.info(s"Removing ${member.address} from the chat list")
      chatters = chatters - member.address
    case EnterRoom(_, remoteNickname) =>
      log.info(s"Thanks for the invitation Mr. $remoteNickname")
    case UserMessage(contents, nickname) =>
      for ((k,v) <- chatters){
        v ! ChatMessage(nickname, contents )
      }
    case ChatMessage(remoteNickname, contents) =>
      log.info(s"[$remoteNickname] says: $contents")
  }
}

class ChatApp(nickname: String, port: Int) extends App {
  val config = ConfigFactory.parseString(
    s"""
       |akka.cluster.roles = ["${nickname}"]
       |akka.remote.artery.canonical.port = $port
       |""".stripMargin)
    .withFallback(ConfigFactory.load("clustering/clustering_exercise.conf"))
  val system = ActorSystem("RTJVMCluster", config)
  val chatActor = system.actorOf(ChatActor.props(nickname, port), "chatActor")
  scala.io.Source.stdin.getLines().foreach { line =>
    chatActor ! UserMessage(line, nickname)
  }

}

object Alice extends ChatApp("Alice", 2551)

object Bob extends ChatApp("Bob", 2552)

object Charlie extends ChatApp("Charlie", 2553)

