package clustering.advanced_clustering

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, PoisonPill, Props, ReceiveTimeout}
import akka.cluster.singleton.{ClusterSingletonManager, ClusterSingletonManagerSettings, ClusterSingletonProxy, ClusterSingletonProxySettings}
import com.typesafe.config.ConfigFactory
import playground.remoting.MySerializer
import sun.security.provider.ConfigFile

import java.util.UUID
import scala.util.Random

case class Person(id: String, age: Int)
object Person{
  def generate() = Person(UUID.randomUUID().toString, 16 + Random.nextInt(90))
}

case class Vote(person: Person, candidate: String) extends MySerializer
case object VoteAccepted  extends MySerializer
case class VoteRejected(reason: String)  extends MySerializer

class VotingAggregator extends Actor with ActorLogging{
  import scala.concurrent.duration._
  import scala.language.postfixOps
  val CANDIDATES: Set[String] = Set("Martin", "Roland", "Jonas", "Daniel")

  context.setReceiveTimeout(2 minutes)

  override def receive: Receive = online(Set(), Map())

  def online(personsVoted: Set[String], polls: Map[String,Int]): Receive = {
    case Vote(Person(id, age), candidate) =>
      if(personsVoted.contains(id)) sender() ! VoteRejected("already voted")
      else if(age < 18) sender() ! VoteRejected("not above legal voting age")
      else if(!CANDIDATES.contains(candidate)) sender() ! VoteRejected("invalid candidate")
      else{
        log.info(s"Recording vote from person $id for $candidate")
        val candidatesVotes = polls.getOrElse(candidate, 0)
        sender() ! VoteAccepted
        context.become(online(personsVoted + id , polls + (candidate -> (candidatesVotes + 1))))
      }
    case ReceiveTimeout =>
      log.info(s"TIME's UP, here are the poll results $polls")
      context.setReceiveTimeout(Duration.Undefined)
      context.become(offline)
  }
  def offline: Receive = {
    case v: Vote =>
      log.warning(s"Received $v, which is invalid as the time is up")
      sender() ! VoteRejected("Cannot accept votes after the polls closing time")
    case m =>
      log.warning(s"Received $m - will not process more messages after polls closing time")
  }

}


object VotingStation{
  def props(votingAggregator: ActorRef) = Props(new VotingStation(votingAggregator))
}
class VotingStation(votingAggregator: ActorRef) extends Actor with ActorLogging{
  override def receive: Receive = {
    case v: Vote => votingAggregator ! v
    case VoteAccepted => log.info("Vote was accepted")
    case VoteRejected(reason) => log.warning(s"Vote was rejected for reason: $reason")
  }
}

object CentralElectionSystem extends App{
  def startNode(port:Int) = {
    val config = ConfigFactory.parseString(
      s"""
        |akka.remote.artery.canonical.port = $port
        |""".stripMargin)
      .withFallback(ConfigFactory.load("advanced_clustering/voting_system_singleton"))
    val system = ActorSystem("RTJVMCluster", config)

    system.actorOf(
      ClusterSingletonManager.props(
        Props[VotingAggregator],
        PoisonPill,
        ClusterSingletonManagerSettings(system)
      ),
      "centralVotingAggregator"
    )
  }

  (2551 to 2553).foreach(startNode)
}

class VotingStationApp(port:Int) extends App {
  val config = ConfigFactory.parseString(
    s"""
       |akka.remote.artery.canonical.port = $port
       |""".stripMargin)
    .withFallback(ConfigFactory.load("advanced_clustering/voting_system_singleton"))
  val system = ActorSystem("RTJVMCluster", config)

  val centralVotingAggregatorProxy = system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/centralVotingAggregator",
      ClusterSingletonProxySettings(system)
    )
  )
  val votingStation = system.actorOf(VotingStation.props(centralVotingAggregatorProxy))
  scala.io.Source.stdin.getLines().foreach { line =>
    votingStation ! Vote(Person.generate(), line)
  }

}

object Washington extends VotingStationApp(2561)
object NewYork extends VotingStationApp(port = 2562)
object SanFrancisco extends VotingStationApp(2563)