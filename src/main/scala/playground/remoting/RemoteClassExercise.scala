//package playground.remoting

import akka.actor.{Actor, ActorIdentity, ActorLogging, ActorRef, ActorSystem, Identify, PoisonPill, Props, Stash}
import akka.serialization.Serializer
import com.typesafe.config.ConfigFactory
//
//class WordCountTaskSerializer extends Serializer {
//
//  import WordCountDomain._
//
//  val SEPARATOR = "//"
//
//  override def identifier: Int = 532154
//
//  override def toBinary(o: AnyRef): Array[Byte] = o match {
//    case event@WordCountTask(text) =>
//      println(s"Serializing $event")
//      s"[$text]".getBytes()
//    case _ => throw new IllegalArgumentException("Not supported in this serializer")
//  }
//
//  override def includeManifest: Boolean = false
//
//  override def fromBinary(bytes: Array[Byte], manifest: Option[Class[_]]): AnyRef = {
//    val string = new String(bytes)
//    val text = string.substring(1, string.length() - 1)
//    WordCountTask(text)
//  }
//}
//
//trait MySerializer
//
//object WordCountDomain {
//  case class Initialize(nWorkers: Int)
//
//  case class WordCountTask(text: String) extends MySerializer
//
//  case class WordCountResult(count: Int) extends MySerializer
//
//  case object EndWordCount
//}
//
//class WordCounterWorker extends Actor with ActorLogging {
//
//  import WordCountDomain._
//
//  override def receive: Receive = {
//    case WordCountTask(text) =>
//      log.info(s"I'm processing: $text")
//      sender() ! WordCountResult(text.split(" ").length)
//  }
//}
//
//class WordCountMaster extends Actor with ActorLogging with Stash {
//
//  import WordCountDomain._
//
//  override def receive: Receive = {
//    case Initialize(nWorkers) =>
//      val startingMap = (1 to nWorkers).toList.map(i => {
//        val actorSelection = context.actorSelection(s"akka://WorkersSystem@localhost:2552/user/wordCountWorker$i")
//        actorSelection ! Identify(i)
//        (i -> false)
//      }).toMap
//      unstashAll()
//      context.become(preStartRequirements(startingMap, Nil))
//    case ActorIdentity =>
//      println("Got an identification but I'm still Sending out requirements")
//      stash()
//  }
//
//  def preStartRequirements(responses: Map[Int, Boolean], actors: List[ActorRef]): Receive = {
//    case ActorIdentity(idNumber: Int, Some(actorRef)) =>
//      println(s"Actor $actorRef with id $idNumber has checked in")
//      actorRef ! s"Thank you actor ${actorRef} for identifying yourself!"
//      val newActors = actorRef :: actors
//      val newResponses: Map[Int, Boolean] = responses + (idNumber -> true)
//      newResponses.collect {
//        case (key, value) if (!value) => (key, value)
//      }.headOption match {
//        case Some(_) =>
//          unstashAll()
//          context.become(preStartRequirements(newResponses, newActors))
//        case None =>
//          unstashAll()
//          println(s"My list of identified actor is $newActors")
//          context.become(online(newActors, 0, 0))
//      }
//    case _ =>
//      println("Im still requiring some actors to identify and received a message")
//      stash()
//  }
//
//  def online(workers: List[ActorRef], remainingTasks: Int, totalCount: Int): Receive = {
//    case text: String =>
//      val sentences = text.split("\\. ")
//      Iterator.continually(workers).flatten.zip(sentences.iterator).foreach { pair =>
//        val (worker, sentence) = pair
//        worker ! WordCountTask(sentence)
//      }
//      context.become(online(workers, remainingTasks + sentences.length, totalCount))
//    case WordCountResult(count) =>
//      if (remainingTasks == 1) {
//        log.info(s"TOTAL RESULT: ${totalCount + count}")
//        workers.foreach(_ ! PoisonPill)
//        context.stop(self)
//      } else {
//        context.become(online(workers, remainingTasks - 1, totalCount + count))
//      }
//  }
//}
//
//object MasterApp extends App {
//  val system = ActorSystem("LocalActorSystem", ConfigFactory.load("remoting/deployingActorsRemotely.conf").getConfig("localApp"))
//  val master = system.actorOf(Props[SimpleActor], "remoteActor")
//
//  import WordCountDomain._
//
//  master ! Initialize(5)
//  Thread.sleep(1000)
//  scala.io.Source.fromFile("src/main/resources/txt/lipsum.txt").getLines().foreach { line =>
//    master ! line
//  }
//}
//
//object WorkersApp extends App {
//
//  val config = ConfigFactory.parseString(
//    """
//      |akka.remote.artery.canonical.port = 2552
//      |""".stripMargin)
//    .withFallback(ConfigFactory.load("remoting/remoteActorsExercise.conf"))
//  val system = ActorSystem("WorkersSystem", config)
//  (1 to 5).map(i => system.actorOf(Props[WordCounterWorker], s"wordCountWorker$i"))
//}