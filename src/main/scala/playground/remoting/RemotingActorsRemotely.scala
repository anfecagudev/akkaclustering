package playground.remoting

import akka.actor.{ActorSystem, Props}
import com.typesafe.config.ConfigFactory

object RemotingActorsRemotely_LocalApp extends App {

  val system = ActorSystem("LocalActorSystem", ConfigFactory.load("remoting/deployingActorsRemotely.conf").getConfig("localApp"))
  val simpleActor = system.actorOf(Props[SimpleActor], "remoteActor")
  simpleActor ! "hello, remote actor!"

}

object RemotingActorsRemotely_RemoteApp extends App {

  val system = ActorSystem("RemoteActorSystem", ConfigFactory.load("remoting/deployingActorsRemotely.conf").getConfig("remoteApp"))
}