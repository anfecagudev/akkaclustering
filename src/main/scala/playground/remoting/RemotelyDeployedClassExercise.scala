package playground.remoting

import akka.actor.{Actor, ActorIdentity, ActorLogging, ActorRef, ActorSystem, Identify, PoisonPill, Props, Stash}
import akka.routing.FromConfig
import akka.serialization.Serializer
import com.typesafe.config.ConfigFactory

class WordCountTaskSerializer extends Serializer {

  import WordCountDomain._

  val SEPARATOR = "//"

  override def identifier: Int = 532154

  override def toBinary(o: AnyRef): Array[Byte] = o match {
    case event@WordCountTask(text) =>
      println(s"Serializing $event")
      s"[$text]".getBytes()
    case _ => throw new IllegalArgumentException("Not supported in this serializer")
  }

  override def includeManifest: Boolean = false

  override def fromBinary(bytes: Array[Byte], manifest: Option[Class[_]]): AnyRef = {
    val string = new String(bytes)
    val text = string.substring(1, string.length() - 1)
    WordCountTask(text)
  }
}

trait MySerializer

object WordCountDomain {
  case class Initialize(nWorkers: Int) extends MySerializer

  case class WordCountTask(text: String) extends MySerializer

  case class WordCountResult(count: Int) extends MySerializer

  case object EndWordCount extends MySerializer
}

class WordCounterWorker extends Actor with ActorLogging {

  import WordCountDomain._

  override def receive: Receive = {
    case WordCountTask(text) =>
      log.info(s"I'm processing: $text")
      sender() ! WordCountResult(text.split(" ").length)
  }
}

class WordCountMaster extends Actor with ActorLogging with Stash {

  import WordCountDomain._

  val workerRouter = context.actorOf(FromConfig.props(Props[WordCounterWorker]), "workerRouter")

  override def receive: Receive = onlineWithRouter(0,0)

  def onlineWithRouter(remainingTasks: Int, totalCount: Int): Receive = {
    case text: String =>
      val sentences = text.split("\\. ")
      sentences.foreach(sentence => workerRouter ! WordCountTask(sentence))
      context.become(onlineWithRouter(remainingTasks + sentences.length, totalCount))
    case WordCountResult(count) =>
      if (remainingTasks == 1) {
        log.info(s"TOTAL RESULT: ${totalCount + count}")
        context.stop(self)
      } else {
        context.become(onlineWithRouter(remainingTasks - 1, totalCount + count))
      }
  }



  def waitingToInitializeWorkers(nWorkers:Int):Receive =
  {
    case Initialize(nWorkers) =>
      val startingList = (1 to nWorkers).toList.map(i => {
        context.actorOf(Props[WordCounterWorker], s"remotelyDeployedActor${i}")
      })
      unstashAll()
      context.become(online(startingList, 0, 0))

  }


  def online(workers: List[ActorRef], remainingTasks: Int, totalCount: Int): Receive = {
    case text: String =>
      val sentences = text.split("\\. ")
      Iterator.continually(workers).flatten.zip(sentences.iterator).foreach { pair =>
        val (worker, sentence) = pair
        worker ! WordCountTask(sentence)
      }
      context.become(online(workers, remainingTasks + sentences.length, totalCount))
    case WordCountResult(count) =>
      if (remainingTasks == 1) {
        log.info(s"TOTAL RESULT: ${totalCount + count}")
        workers.foreach(_ ! PoisonPill)
        context.stop(self)
      } else {
        context.become(online(workers, remainingTasks - 1, totalCount + count))
      }
  }
}

object MasterApp extends App {

  import WordCountDomain._

  val config = ConfigFactory.parseString(
    """
      |akka.remote.artery.canonical.port = 2551
      |""".stripMargin)
    .withFallback(ConfigFactory.load("remoting/deployingActorsRemotelyExercise.conf"))
  val system = ActorSystem("MasterSystem", config)
  val master = system.actorOf(Props[WordCountMaster], "wordCountMaster")
  master ! Initialize(5)
  Thread.sleep(1000)
  scala.io.Source.fromFile("src/main/resources/txt/lipsum.txt").getLines().foreach { line =>
    master ! line
  }
}

object WorkersApp extends App {
  val config = ConfigFactory.parseString(
    """
      |akka.remote.artery.canonical.port = 2552
      |""".stripMargin)
    .withFallback(ConfigFactory.load("remoting/deployingActorsRemotelyExercise.conf"))
  val system = ActorSystem("WorkersSystem", config)
}